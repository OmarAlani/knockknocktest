﻿namespace RedPillService.Tests
{
    using System;
    using System.ServiceModel;
    using knockknock.readify.net;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Here, I am passing the same values that Readify tests use to test my service.
    /// </summary>
    [TestClass]
    public class RedPillServiceTests
    {
        // Note: These are the tests values that readify service is running against my service
        // Fibonacci Tests
        // -4, -5, 0, 1, -6, 3, 4, 5, 6, 7, 46, 47, 92, 2, -3, -2, -1, -92, -47 
        // -46, -7, -93, 93, -9223372036854775808, -2147483648, 2147483647, 9223372036854775807

        #region Fibonacci Tests

        [TestMethod]
        public void PassingMinus4FibonacciNumberShouldBeMinus3()
        {
            // Arrange
            const long n = -4;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(-3, fib);
        }

        [TestMethod]
        public void PassingMinus5FibonacciNumberShouldBe5()
        {
            // Arrange
            const long n = -5;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(5, fib);
        }

        [TestMethod]
        public void PassingMinus10FibonacciNumberShouldBeMinus55()
        {
            // Arrange
            const long n = -10;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(-55, fib);
        }

        [TestMethod]
        public void Passing0FibonacciNumberShouldBe0()
        {
            // Arrange
            const long n = 0;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(0, fib);
        }

        [TestMethod]
        public void Passing1FibonacciNumberShouldBe1()
        {
            // Arrange
            const long n = 1;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(1, fib);
        }

        [TestMethod]
        public void PassingMinus6FibonacciNumberShouldBeMinus8()
        {
            // Arrange
            const long n = -6;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(-8, fib);
        }

        [TestMethod]
        public void Passing3FibonacciNumberShouldBe2()
        {
            // Arrange
            const long n = 3;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(2, fib);
        }

        [TestMethod]
        public void Passing4FibonacciNumberShouldBe3()
        {
            // Arrange
            const long n = 4;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(3, fib);
        }

        [TestMethod]
        public void Passing5FibonacciNumberShouldBe5()
        {
            // Arrange
            const long n = 5;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(5, fib);
        }

        [TestMethod]
        public void Passing6FibonacciNumberShouldBe8()
        {
            // Arrange
            const long n = 6;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(8, fib);
        }

        [TestMethod]
        public void Passing7FibonacciNumberShouldBe13()
        {
            // Arrange
            const long n = 7;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(13, fib);
        }

        [TestMethod]
        public void Passing46FibonacciNumberShouldBe1836311903()
        {
            // Arrange
            const long n = 46;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(1836311903, fib);
        }

        [TestMethod]
        public void Passing47FibonacciNumberShouldBe2971215073()
        {
            // Arrange
            const long n = 47;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(2971215073, fib);
        }

        [TestMethod]
        public void Passing92FibonacciNumberShouldBe7540113804746346429()
        {
            // Arrange
            const long n = 92;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(7540113804746346429, fib);
        }

        [TestMethod]
        public void Passing2FibonacciNumberShouldBe1()
        {
            // Arrange
            const long n = 2;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(1, fib);
        }

        [TestMethod]
        public void PassingMinus3FibonacciNumberShouldBe2()
        {
            // Arrange
            const long n = -3;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(2, fib);
        }

        [TestMethod]
        public void PassingMinus2FibonacciNumberShouldBeMinus1()
        {
            // Arrange
            const long n = -2;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(-1, fib);
        }

        [TestMethod]
        public void PassingMinus1FibonacciNumberShouldBe1()
        {
            // Arrange
            const long n = -1;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(1, fib);
        }

        [TestMethod]
        public void PassingMinus92FibonacciNumberShouldBeMinus7540113804746346429()
        {
            // Arrange
            const long n = -92;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(-7540113804746346429, fib);
        }

        [TestMethod]
        public void PassingMinus47FibonacciNumberShouldBe2971215073()
        {
            // Arrange
            const long n = -47;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(2971215073, fib);
        }

        [TestMethod]
        public void PassingMinus46FibonacciNumberShouldBeMinus1836311903()
        {
            // Arrange
            const long n = -46;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(-1836311903, fib);
        }

        [TestMethod]
        public void PassingMinus7FibonacciNumberShouldBe13()
        {
            // Arrange
            const long n = -7;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(13, fib);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<ArgumentOutOfRangeException>))]
        public void PassingMinus93FibonacciNumberShouldRaiseFaultExceptionArgumentOutOfRangeException()
        {
            // Arrange
            const long n = -93;

            // Act
            var service = new RedPillClient();

            service.FibonacciNumber(n);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<ArgumentOutOfRangeException>))]
        public void Passing93ShouldRaiseFaultExceptionArgumentOutOfRangeException()
        {
            // Arrange
            const long n = 93;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(-55, fib);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<ArgumentOutOfRangeException>))]
        public void PassingMins9223372036854775808ShouldRaiseFaultExceptionArgumentOutOfRangeException()
        {
            // Arrange
            const long n = -9223372036854775808;

            // Act
            var service = new RedPillClient();

            service.FibonacciNumber(n);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<ArgumentOutOfRangeException>))]
        public void PassingMins2147483648ShouldRaiseFaultExceptionArgumentOutOfRangeException()
        {
            // Arrange
            const long n = -2147483648;

            // Act
            var service = new RedPillClient();

            service.FibonacciNumber(n);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException<ArgumentOutOfRangeException>))]
        public void Passing9223372036854775807ShouldRaiseFaultExceptionArgumentOutOfRangeException()
        {
            // Arrange
            const long n = 9223372036854775807;

            // Act
            var service = new RedPillClient();

            var fib = service.FibonacciNumber(n);

            // Assert
            Assert.AreEqual(-55, fib);
        }

        #endregion

        // Note: These are the tests values that readify service is running against my service
        // ReverseWords Tests
        //NULL
        //cat
        //trailing space 
        //Bang!

        //cat and dog
        //two  spaces
        // leading space
        //Capital
        //This is a snark: ?
        //P!u@n#c$t%u^a&t*i(o)n
        //detartrated kayak detartrated
        //¿Qué?
        //  S  P  A  C  E  Y  
        //!B!A!N!G!S!

        #region ReverseWords Tests

        [TestMethod]
        [ExpectedException(typeof(FaultException<ArgumentNullException>))]
        public void PassNullShouldRaiseFaultExceptionArgumentNullException()
        {
            // Arrange
            const string words = null;

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual(words, reversedWords);
        }

        [TestMethod]
        public void Passingcat()
        {
            // Arrange
            const string words = "cat";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("tac", reversedWords);
        }

        [TestMethod]
        public void PassingTrailingSpace()
        {
            // Arrange
            const string words = "trailing space ";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("gniliart ecaps ", reversedWords);
        }

        [TestMethod]
        public void PassingBang()
        {
            // Arrange
            const string words = "Bang!";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("!gnaB", reversedWords);
        }

        [TestMethod]
        public void PassingEmptyString()
        {
            // Arrange
            const string words = "";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual(words, reversedWords);
        }

        [TestMethod]
        public void PassingCatAndDog()
        {
            // Arrange
            const string words = "cat and dog";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("tac dna god", reversedWords);
        }

        [TestMethod]
        public void PassingTwoSpaces()
        {
            // Arrange
            const string words = "two  spaces";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("owt  secaps", reversedWords);
        }

        [TestMethod]
        public void PassingLeadingSpaces()
        {
            // Arrange
            const string words = " leading space";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual(" gnidael ecaps", reversedWords);
        }

        [TestMethod]
        public void PassingCapital()
        {
            // Arrange
            const string words = "Capital";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("latipaC", reversedWords);
        }

        [TestMethod]
        public void PassingThisIsASnack()
        {
            // Arrange
            const string words = "This is a snark: ?";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("sihT si a :krans ?", reversedWords);
        }

        [TestMethod]
        public void PassingSpecialChars()
        {
            // Arrange
            const string words = "P!u@n#c$t%u^a&t*i(o)n";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("n)o(i*t&a^u%t$c#n@u!P", reversedWords);
        }

        [TestMethod]
        public void PassingDetartratedKayakDetartrated()
        {
            // Arrange
            const string words = "detartrated kayak detartrated";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual(words, reversedWords);
        }

        [TestMethod]
        public void PassingQué()
        {
            // Arrange
            const string words = "¿Qué?";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("?éuQ¿", reversedWords);
        }

        [TestMethod]
        public void PassingSPACEY()
        {
            // Arrange
            const string words = "  S  P  A  C  E  Y  ";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual(words, reversedWords);
        }

        [TestMethod]
        public void PassingBANGS()
        {
            // Arrange
            const string words = "!B!A!N!G!S!";

            var service = new RedPillClient();

            var reversedWords = service.ReverseWords(words);

            // Assert
            Assert.AreEqual("!S!G!N!A!B!", reversedWords);
        }
        #endregion

        // Note: These are the tests values that readify service is running against my service
        // WhatShapeIsThis Tests
        //0	0	0, - 
        //1	1	0 - 
        //1	1	2 - 
        //1	0	1 - 
        //2	1	1 - 
        //1	2	3 - 
        //1	1	2147483647 - 
        //1	1	1 -
        //2	2	2 - 
        //2147483647	2147483647	2147483647 - 
        //2	2	3 - 
        //2	3	2 - 
        //3	2	2 - 
        //2	3	4 - 
        //3	4	2 - 
        //4	2	3 - 
        //4	3	2 -
        //1	2	1 -
        //-2147483648	-2147483648	-2147483648
        //-1	-1	-1
        //-1	1	1
        //1	-1	1
        //1	1	-1
        //0	1	1
        //2147483647	2147483647	2147483647
        #region WhatShapeIsThis Tests
        [TestMethod]
        public void Pass0And0And0ShouldReturnError()
        {
            // Arrange
            const int a = 0;
            const int b = 0;
            const int c = 0;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass1And1And0ShouldReturnError()
        {
            // Arrange
            const int a = 1;
            const int b = 1;
            const int c = 0;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass1And1And2ShouldReturnError()
        {
            // Arrange
            const int a = 1;
            const int b = 1;
            const int c = 2;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass1And0And1ShouldReturnError()
        {
            // Arrange
            const int a = 1;
            const int b = 0;
            const int c = 1;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass2And1And1ShouldReturnError()
        {
            // Arrange
            const int a = 2;
            const int b = 1;
            const int c = 1;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass1And2And3ShouldReturnError()
        {
            // Arrange
            const int a = 1;
            const int b = 2;
            const int c = 3;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass1And2And2147483647ShouldReturnError()
        {
            // Arrange
            const int a = 1;
            const int b = 2;
            const int c = 2147483647;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass1And1And1ShouldReturnEquilateral()
        {
            // Arrange
            const int a = 1;
            const int b = 1;
            const int c = 1;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Equilateral, type);
        }

        [TestMethod]
        public void Pass2And2And2ShouldReturnEquilateral()
        {
            // Arrange
            const int a = 2;
            const int b = 2;
            const int c = 2;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Equilateral, type);
        }

        [TestMethod]
        public void Pass2147483647And2147483647And2147483647ShouldReturnEquilateral()
        {
            // Arrange
            const int a = 2147483647;
            const int b = 2147483647;
            const int c = 2147483647;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Equilateral, type);
        }

        [TestMethod]
        public void Pass2And2And3ShouldReturnIsosceles()
        {
            // Arrange
            const int a = 2;
            const int b = 2;
            const int c = 3;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Isosceles, type);
        }

        [TestMethod]
        public void Pass2And3And2ShouldReturnIsosceles()
        {
            // Arrange
            const int a = 2;
            const int b = 3;
            const int c = 2;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Isosceles, type);
        }

        [TestMethod]
        public void Pass3And2And2ShouldReturnIsosceles()
        {
            // Arrange
            const int a = 3;
            const int b = 2;
            const int c = 2;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Isosceles, type);
        }


        [TestMethod]
        public void Pass2And3And4ShouldReturnScalene()
        {
            // Arrange
            const int a = 2;
            const int b = 3;
            const int c = 4;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Scalene, type);
        }

        [TestMethod]
        public void Pass3And4And2ShouldReturnScalene()
        {
            // Arrange
            const int a = 3;
            const int b = 4;
            const int c = 2;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Scalene, type);
        }

        [TestMethod]
        public void Pass4And2And3ShouldReturnScalene()
        {
            // Arrange
            const int a = 4;
            const int b = 2;
            const int c = 3;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Scalene, type);
        }

        [TestMethod]
        public void Pass4And3And2ShouldReturnScalene()
        {
            // Arrange
            const int a = 4;
            const int b = 3;
            const int c = 2;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Scalene, type);
        }

        [TestMethod]
        public void Pass1And2And1ShouldReturnError()
        {
            // Arrange
            const int a = 1;
            const int b = 2;
            const int c = 1;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void PassMin2147483648AndMin2147483648AndMin2147483648ShouldReturnError()
        {
            // Arrange
            const int a = -2147483648;
            const int b = -2147483648;
            const int c = -2147483648;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void PassMin1AndMin1AndMin1ShouldReturnError()
        {
            // Arrange
            const int a = -1;
            const int b = -1;
            const int c = -1;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void PassMin1And1And1ShouldReturnError()
        {
            // Arrange
            const int a = -1;
            const int b = 1;
            const int c = 1;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass1AndMin1And1ShouldReturnError()
        {
            // Arrange
            const int a = 1;
            const int b = -1;
            const int c = 1;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass1And1AndMin1ShouldReturnError()
        {
            // Arrange
            const int a = 1;
            const int b = 1;
            const int c = -1;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        [TestMethod]
        public void Pass0And1AndMin1ShouldReturnError()
        {
            // Arrange
            const int a = 0;
            const int b = 1;
            const int c = -1;

            var service = new RedPillClient();

            // Act
            var type = service.WhatShapeIsThis(a, b, c);

            // Assert
            Assert.AreEqual(TriangleType.Error, type);
        }

        #endregion
    }
}
