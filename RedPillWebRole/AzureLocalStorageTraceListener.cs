namespace RedPillWebRole
{
    using System.Diagnostics;
    using System.IO;
    using Microsoft.WindowsAzure.Diagnostics;
    using Microsoft.WindowsAzure.ServiceRuntime;

    public class AzureLocalStorageTraceListener : XmlWriterTraceListener
    {
        public AzureLocalStorageTraceListener()
            : base(Path.Combine(AzureLocalStorageTraceListener.GetLogDirectory().Path, "RedPillWebRole.svclog"))
        {
        }

        public static DirectoryConfiguration GetLogDirectory()
        {
            var directory = new DirectoryConfiguration
            {
                Container = "wad-tracefiles",
                DirectoryQuotaInMB = 10,
                Path = RoleEnvironment.GetLocalResource("RedPillWebRole.svclog").RootPath
            };
            return directory;
        }
    }
}
