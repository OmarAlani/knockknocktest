﻿namespace RedPillWebRole
{
    using System;
    using System.ServiceModel;
    using RedPillWebRole.Helpers;

    public class RedPill : IRedPill
    {
        public Guid WhatIsYourToken()
        {
            return new Guid("7dfe4dea-b2af-402e-928b-e88b0f320eb0");
        }

        public long FibonacciNumber(long n)
        {
            if (n < -92)
            {
                var ex = new ArgumentOutOfRangeException("n");
                throw new FaultException<ArgumentOutOfRangeException>(ex, new FaultReason("Fib(<92) will cause a 64-bit integer overflow."));
            }

            if (n > 92)
            {
                var ex = new ArgumentOutOfRangeException("n");
                throw new FaultException<ArgumentOutOfRangeException>(ex, new FaultReason("Fib(>92) will cause a 64-bit integer overflow."));
            }

            return n >= 0
                ? FibonacciHelper.Fibonacci(n)
                : FibonacciHelper.NegativeFibonacci(n);
        }

        public TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            return TriangleHelper.GetTriangleType(a, b, c);
        }

        public string ReverseWords(string str)
        {
            if (str == null)
            {
                var ex = new ArgumentNullException("str");
                throw new FaultException<ArgumentNullException>(ex, "Value cannot be null");
            }

            return str.ReverseWords();
        }
    }
}
