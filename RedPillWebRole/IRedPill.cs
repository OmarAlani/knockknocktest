﻿namespace RedPillWebRole
{
    using System;
    using System.ServiceModel;

    [ServiceContract(Namespace = "http://KnockKnock.readify.net")]
    public interface IRedPill
    {
        [OperationContract(Name = "WhatIsYourToken")]
        Guid WhatIsYourToken();

        [OperationContract(Name = "FibonacciNumber")]
        [FaultContract(typeof(ArgumentOutOfRangeException))]
        long FibonacciNumber(long n);

        [OperationContract(Name = "WhatShapeIsThis")]
        TriangleType WhatShapeIsThis(int a, int b, int c);

        [OperationContract(Name = "ReverseWords")]
        [FaultContract(typeof(ArgumentNullException))]
        string ReverseWords(string s);
    }
}