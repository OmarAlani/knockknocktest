namespace RedPillWebRole.Helpers
{
    using System;
    using System.Collections.Generic;

    internal static class FibonacciHelper
    {
        private static readonly Dictionary<long, long> FibonacciCache;

        static FibonacciHelper()
        {
            FibonacciCache = new Dictionary<long, long>();
        }

        public static long Fibonacci(long n)
        {
            if (FibonacciCache.ContainsKey(n))
            {
                return FibonacciCache[n];
            }

            if (n == 0)
            {
                return 0;
            }

            if (n == 1 || n == 2)
            {
                return 1;
            }

            var fibonacciNumber = Fibonacci(n - 1) + Fibonacci(n - 2);

            FibonacciCache.Add(n, fibonacciNumber);

            return fibonacciNumber;
        }

        public static long NegativeFibonacci(long number)
        {
            var absoluteNumber = Math.Abs(number);

            if (absoluteNumber % 2 == 1)
            {
                return Fibonacci(absoluteNumber);
            }

            return Fibonacci(absoluteNumber) * -1;
        }
    }
}