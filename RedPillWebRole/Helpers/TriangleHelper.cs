﻿namespace RedPillWebRole.Helpers
{
    public class TriangleHelper
    {
        public static TriangleType GetTriangleType(int a, int b, int c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
            {
                return TriangleType.Error;
            }

            if (a == b && a == c)
            {
                return TriangleType.Equilateral;
            }

            if (a == b || b == c || a == c)
            {
                if (IsValidTriangle(a, b, c))
                {
                    return TriangleType.Isosceles;
                }

                return TriangleType.Error;
            }

            return IsValidTriangle(a, b, c)
                ? TriangleType.Scalene
                : TriangleType.Error;
        }

        private static bool IsValidTriangle(int a, int b, int c)
        {
            return (a + b > c && a + c > b && b + c > a);
        }
    }
}