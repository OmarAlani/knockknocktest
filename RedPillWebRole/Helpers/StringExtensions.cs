﻿namespace RedPillWebRole.Helpers
{
    using System.Linq;
    using System.Text;

    public static class StringExtensions
    {
        public static string ReverseWords(this string str)
        {
            if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
            {
                return str;
            }

            var sb = new StringBuilder();

            var chars = str.ToCharArray();

            for (var i = 0; i < chars.Length; i++)
            {
                if (chars[i] == ' ')
                {
                    sb.Append(chars[i]);
                }
                else
                {
                    var wordsb = new StringBuilder();
                    do
                    {
                        wordsb.Append(chars[i]);
                        i++;
                    } while (i < chars.Length && chars[i] != ' ');

                    sb.Append(wordsb.ToString().Reverse().ToArray());

                    if (i < chars.Length)
                    {
                        sb.Append(' ');
                    }
                }
            }

            return sb.ToString();
        }
    }
}