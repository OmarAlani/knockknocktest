﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="RedPill" generation="1" functional="0" release="0" Id="9840c20b-f08c-4283-bc39-90c6f739850a" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="RedPillGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="RedPillWebRole:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/RedPill/RedPillGroup/LB:RedPillWebRole:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="RedPillWebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/RedPill/RedPillGroup/MapRedPillWebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="RedPillWebRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/RedPill/RedPillGroup/MapRedPillWebRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:RedPillWebRole:Endpoint1">
          <toPorts>
            <inPortMoniker name="/RedPill/RedPillGroup/RedPillWebRole/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapRedPillWebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/RedPill/RedPillGroup/RedPillWebRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapRedPillWebRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/RedPill/RedPillGroup/RedPillWebRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="RedPillWebRole" generation="1" functional="0" release="0" software="c:\users\omar.oalani\documents\visual studio 2013\Projects\RedPill\RedPill\csx\Release\roles\RedPillWebRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;RedPillWebRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;RedPillWebRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="RedPillWebRole.svclog" defaultAmount="[1000,1000,1000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/RedPill/RedPillGroup/RedPillWebRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/RedPill/RedPillGroup/RedPillWebRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/RedPill/RedPillGroup/RedPillWebRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="RedPillWebRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="RedPillWebRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="RedPillWebRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="7fc7dcd2-edb4-4646-8535-0c04d141fbeb" ref="Microsoft.RedDog.Contract\ServiceContract\RedPillContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="4ca65d1d-b952-4cdb-9ce1-f71872bd155c" ref="Microsoft.RedDog.Contract\Interface\RedPillWebRole:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/RedPill/RedPillGroup/RedPillWebRole:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>